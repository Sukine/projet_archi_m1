package authentification;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.m1projetachitecture.R;
import com.google.android.material.textfield.TextInputEditText;

import bdd.DatabaseRequest;
import bdd.VolleySingleton;
import navigation.MainApp;
import model.SessionManager;
import model.User;

// Activité de connexion
public class LogIn extends AppCompatActivity {

    private TextInputEditText mEmailEditText;
    private TextInputEditText mPasswordEditText;
    private TextView mRegisterTextView;
    private Button mLogInButton;

    private User mUser;

    private RequestQueue queue;
    private DatabaseRequest request;

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //On récupère les champs de la vue
        mEmailEditText = findViewById(R.id.login_textinputedittext_email);
        mPasswordEditText = findViewById(R.id.login_textinputedittext_password);
        mRegisterTextView = findViewById(R.id.login_textview_register);
        mLogInButton = findViewById(R.id.login_button_login);

        sessionManager = new SessionManager(this);
        //Si l'utilisateur est déjà connecté, pas besoin de se reconnecter
        if(sessionManager.isLogged()){
            Intent mainAppIntent = new Intent(LogIn.this, MainApp.class);
            startActivity(mainAppIntent);
            finish();
        }

        //Permet de ne pas réinstancier à chaque fois la RequestQueue et d'en avoir qu'une seule pour toute l'application
        queue = VolleySingleton.getInstance(this).getRequestQueue();
        request = new DatabaseRequest(this,queue);

        //On définit l'action du bouton de connexion
        mLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mEmail = mEmailEditText.getText().toString().trim();
                String mPassword = mPasswordEditText.getText().toString().trim();

                //On construit la requête au serveur
                request.login(mEmail, mPassword, new DatabaseRequest.LoginCallback() {
                    @Override
                    public void onSuccess(String message, User user) {
                        //On met à jour les données de session avec les données de l'utilisateur
                        sessionManager.logUser();
                        sessionManager.insertId(""+user.getmIdentifiant());
                        sessionManager.insertPrenom(user.getfirstName());
                        sessionManager.insertNom(user.getmLastName());
                        sessionManager.insertBirthdate(user.getmBirthDate());
                        sessionManager.insertPhoneNb(user.getmPhoneNb());
                        sessionManager.insertAddress(user.getmAddress());
                        sessionManager.insertEmail(user.getmEmail());
                        sessionManager.insertPassword(user.getPassword());

                        Intent mainAppIntent = new Intent(LogIn.this, MainApp.class);
                        mainAppIntent.putExtra("CONNEXION",message);
                        startActivity(mainAppIntent);
                        finish();
                    }

                    @Override
                    public void onError(String message) {
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        //On définit l'action du lien d'inscription
        mRegisterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LogIn.this, Register.class);
                startActivity(registerIntent);
                finish();
            }
        });

    }



}