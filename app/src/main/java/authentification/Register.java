package authentification;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.m1projetachitecture.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Map;

import bdd.DatabaseRequest;
import bdd.VolleySingleton;
import navigation.MainApp;
import model.SessionManager;
import model.User;

public class Register extends AppCompatActivity {

    private TextInputLayout mFirstNameEditText;
    private TextInputLayout mLastNameEditText;
    private TextInputLayout mBirthDateEditText;
    private TextInputLayout mPhoneNbEditText;
    private TextInputLayout mAddressEditText;
    private TextInputLayout mEmailEditText;
    private TextInputLayout mPasswordEditText;
    private TextInputLayout mPasswordConfirmEditText;
    private CheckBox mCheckBoxRulesConsent;
    private TextView mCheckBoxRulesConsentError;
    private Button mRegisterButton;
    private TextView mLogInTextView;

    private User mUser;

    //POUR LA BDD--------------
    private RequestQueue queue;
    private DatabaseRequest request;
    //-------------------------

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mFirstNameEditText = findViewById(R.id.register_textinputlayout_first_name);
        mLastNameEditText = findViewById(R.id.register_textinputlayout_last_name);
        mBirthDateEditText = findViewById(R.id.register_textinputlayout_birth_date);
        mPhoneNbEditText = findViewById(R.id.register_textinputlayout_phone_nb);
        mAddressEditText = findViewById(R.id.register_textinputlayout_address);
        mEmailEditText = findViewById(R.id.register_textinputlayout_email);
        mPasswordEditText = findViewById(R.id.register_textinputlayout_password);
        mPasswordConfirmEditText = findViewById(R.id.register_textinputlayout_confirm_password);
        mCheckBoxRulesConsent = findViewById(R.id.register_checkbox_rules_consent);
        mRegisterButton = findViewById(R.id.register_button_register);
        mCheckBoxRulesConsentError = findViewById(R.id.register_checkbox_rules_error);
        mLogInTextView = findViewById(R.id.register_textview_login);

        sessionManager = new SessionManager(this);

        //Permet de ne pas réinstancier à chaque fois la RequestQueue et d'en avoir qu'une seule pour toute l'application
        queue = VolleySingleton.getInstance(this).getRequestQueue();
        request = new DatabaseRequest(this, queue);


        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String prenom = mFirstNameEditText.getEditText().getText().toString().trim();
                String nom = mLastNameEditText.getEditText().getText().toString().trim();
                String birthdate = mBirthDateEditText.getEditText().getText().toString().trim();
                String phone = mPhoneNbEditText.getEditText().getText().toString().trim();
                String address = mAddressEditText.getEditText().getText().toString().trim();
                String mail = mEmailEditText.getEditText().getText().toString().trim();
                String password = mPasswordEditText.getEditText().getText().toString().trim();
                String passwordConfirm = mPasswordConfirmEditText.getEditText().getText().toString().trim();

                request.register(prenom, nom, birthdate, phone, address, mail, password, passwordConfirm, new DatabaseRequest.RegisterCallback() {

                    @Override
                    public void onSuccess(String message, User user) {
                        //On met à jour les données de session avec les données de l'utilisateur
                        sessionManager.logUser();
                        sessionManager.insertId("" + user.getmIdentifiant());
                        sessionManager.insertPrenom(user.getfirstName());
                        sessionManager.insertNom(user.getmLastName());
                        sessionManager.insertBirthdate(user.getmBirthDate());
                        sessionManager.insertPhoneNb(user.getmPhoneNb());
                        sessionManager.insertAddress(user.getmAddress());
                        sessionManager.insertEmail(user.getmEmail());
                        sessionManager.insertPassword(user.getPassword());


                        Intent mainAppIntent = new Intent(Register.this, MainApp.class);
                        mainAppIntent.putExtra("CONNEXION", message);
                        startActivity(mainAppIntent);
                        finish();
                    }

                    @Override
                    public void inputError(Map<String, String> errors) {
                        if (errors.get("prenom") != null) {
                            mFirstNameEditText.setError(errors.get("prenom"));
                        } else {
                            mFirstNameEditText.setErrorEnabled(false);
                        }
                        if (errors.get("nom") != null) {
                            mLastNameEditText.setError(errors.get("nom"));
                        } else {
                            mLastNameEditText.setErrorEnabled(false);
                        }
                        if (errors.get("birthdate") != null) {
                            mBirthDateEditText.setError(errors.get("birthdate"));
                        } else {
                            mBirthDateEditText.setErrorEnabled(false);
                        }
                        if (errors.get("phone") != null) {
                            mPhoneNbEditText.setError(errors.get("phone"));
                        } else {
                            mPhoneNbEditText.setErrorEnabled(false);
                        }
                        if (errors.get("address") != null) {
                            mAddressEditText.setError(errors.get("address"));
                        } else {
                            mAddressEditText.setErrorEnabled(false);
                        }
                        if (errors.get("mail") != null) {
                            mEmailEditText.setError(errors.get("mail"));
                        } else {
                            mEmailEditText.setErrorEnabled(false);
                        }
                        if (errors.get("password") != null) {
                            mPasswordEditText.setError(errors.get("password"));
                        } else {
                            mPasswordEditText.setErrorEnabled(false);
                        }
                        if (errors.get("passwordConfirm") != null) {
                            mPasswordConfirmEditText.setError(errors.get("passwordConfirm"));
                        } else {
                            mPasswordConfirmEditText.setErrorEnabled(false);
                        }
                    }

                    @Override
                    public void onError(String message) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        mLogInTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent logInIntent = new Intent(Register.this, LogIn.class);
                startActivity(logInIntent);
                finish();
            }
        });

    }
}