package model;

public class ShopItem {

    private int mId;
    private String mName;
    private double mPrice;
    private int mAmountSelected;
    private Shop mShop;
    private String mDescription;

    public ShopItem(int id,String name, double price, String description, Shop shop) {
        this.mId = id;
        mName = name;
        mPrice = price;
        mAmountSelected = 0;
        mShop = shop;
        this.mDescription = description;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getAmountSelected() {
        return mAmountSelected;
    }

    public void setAmountSelected(int amountSelected) {
        mAmountSelected = amountSelected;
    }

    public Shop getShop() {
        return mShop;
    }

    public void setShop(Shop shop) {
        mShop = shop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShopItem)) return false;
        ShopItem shopItem = (ShopItem) o;
        return Double.compare(shopItem.mPrice, mPrice) == 0 && Double.compare(shopItem.mAmountSelected, mAmountSelected) == 0 && mName.equals(shopItem.mName);
    }

    @Override
    public String toString() {
        return "ShopItems{" +
                "mName='" + mName + '\'' +
                ", mPrice=" + mPrice +
                ", mAmountSelected=" + mAmountSelected +
                ", mShop=" + mShop.getName() +
                '}';
    }
}
