package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class Shop implements Parcelable {

    private int id;
    private String name;
    private String address;
    private String description;
    private double lat;
    private double lon;

    public Shop(int id,String name, String address, String description, double lat, double lon) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.description = description;
        this.lat = lat;
        this.lon = lon;
    }

    protected Shop(Parcel in) {
        name = in.readString();
        address = in.readString();
        description = in.readString();
        lat = Double.parseDouble(in.readString());
        lon = Double.parseDouble(in.readString());
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() { return id; }

    public void setId(int identifiant) { this.id = identifiant; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(description);
        dest.writeString(Double.toString(lat));
        dest.writeString(Double.toString(lon));
    }

    @Override
    public String toString() {
        return "Shop{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shop)) return false;
        Shop shop = (Shop) o;
        return id == shop.id && Double.compare(shop.lat, lat) == 0 && Double.compare(shop.lon, lon) == 0 && name.equals(shop.name) && address.equals(shop.address) && Objects.equals(description, shop.description);
    }
}
