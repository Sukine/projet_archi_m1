package model;

public class User {

    private int mIdentifiant;
    private String mfirstName;
    private String mLastName;
    private String mBirthDate;
    private String mPhoneNb;
    private String mAddress;
    private String mEmail;
    private String mPassword;

    public User(int mIdentifiant, String mfirstName, String mLastName, String mBirthDate, String mPhoneNb, String mAddress, String mEmail, String mPassword) {
        this.mIdentifiant = mIdentifiant;
        this.mfirstName = mfirstName;
        this.mLastName = mLastName;
        this.mBirthDate = mBirthDate;
        this.mPhoneNb = mPhoneNb;
        this.mAddress = mAddress;
        this.mEmail = mEmail;
        this.mPassword = mPassword;
    }

    public int getmIdentifiant() { return mIdentifiant; }

    public void setmIdentifiant(int mIdentifiant) { this.mIdentifiant = mIdentifiant; }

    public String getmLastName() { return mLastName; }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmBirthDate() {
        return mBirthDate;
    }

    public void setmBirthDate(String mBirthDate) {
        this.mBirthDate = mBirthDate;
    }

    public String getmPhoneNb() {
        return mPhoneNb;
    }

    public void setmPhoneNb(String mPhoneNb) {
        this.mPhoneNb = mPhoneNb;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getfirstName() {
        return mfirstName;
    }

    public void setfirstName(String mfirstName) {
        this.mfirstName = mfirstName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }


}
