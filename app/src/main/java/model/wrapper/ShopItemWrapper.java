package model.wrapper;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.m1projetachitecture.R;

public class ShopItemWrapper {
    private View base;
    private Button mButtonMore;
    private Button mButtonLess;
    private Button mButtonAddToCart;
    private TextView mAmountSelected;

    public ShopItemWrapper(View base)
    {
        this.base = base;
    }

    public Button getButtonMore()
    {
        if (mButtonMore == null)
        {
            mButtonMore = (Button) base.findViewById(R.id.adapterShopItem_button_more);
        }
        return (mButtonMore);
    }

    public Button getButtonLess()
    {
        if (mButtonLess == null)
        {
            mButtonLess = (Button) base.findViewById(R.id.adapterShopItem_button_less);
        }
        return (mButtonLess);
    }

    public TextView getAmountSelected()
    {
        if (mAmountSelected == null)
        {
            mAmountSelected = (TextView) base.findViewById(R.id.adapterShopItem_textview_amount);
        }
        return (mAmountSelected);
    }

    public Button getButtonAddToCart() {
        if (mButtonAddToCart == null)
        {
            mButtonAddToCart = (Button) base.findViewById(R.id.adapterShopItem_button_add);
        }
        return (mButtonAddToCart);
    }

    public void setButtonAddToCart(Button buttonAddToCart) {
        mButtonAddToCart = buttonAddToCart;
    }
}
