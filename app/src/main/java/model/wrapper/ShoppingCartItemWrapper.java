package model.wrapper;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.m1projetachitecture.R;

public class ShoppingCartItemWrapper {

    private View mBase;
    private Button mDeleteButton;

    public ShoppingCartItemWrapper(View base) {
        this.mBase = base;
    }

    public View getBase() {
        return mBase;
    }

    public void setBase(View base) {
        this.mBase = base;
    }

    public Button getDeleteButton() {
        if (mDeleteButton == null)
        {
            mDeleteButton = (Button) mBase.findViewById(R.id.shoppingCartItem_button_delete);
        }
        return (mDeleteButton);
    }

    public void setDeleteButton(Button deleteButton) {
        this.mDeleteButton = deleteButton;
    }

}
