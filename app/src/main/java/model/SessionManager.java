package model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionManager {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private final static String PREFS_NAME = "app_prefs";
    private final static int PRIVATE_MODE = 0;
    private final static String IS_LOGGED = "isLogged";
    private final static String ID = "-1";
    private final static String PRENOM = "prenom";
    private final static String NOM = "nom";
    private final static String BIRTHDATE = "birthdate";
    private final static String PHONE = "phone";
    private final static String ADDRESS = "address";
    private final static String EMAIL = "email";
    private final static String PASSWORD = "password";
    private Context context;

    @SuppressLint("WrongConstant")
    public SessionManager(Context context){
        this.context = context;
        prefs = context.getSharedPreferences(PREFS_NAME,PRIVATE_MODE);
        editor = prefs.edit();
    }

    public boolean isLogged() {
        return prefs.getBoolean(IS_LOGGED,false);
    }

    public void logout(){
        editor.clear().commit();
    }

    public void logUser() {
        editor.putBoolean(IS_LOGGED,true);
        editor.commit();
    }

    public void insertId(String id){
        editor.putString(ID,id);
        editor.commit();
    }

    public String getId(){
        return prefs.getString(ID,null);
    }

    public void insertPrenom(String prenom){
        editor.putString(PRENOM,prenom);
        editor.commit();
    }

    public String getPrenom(){
        return prefs.getString(PRENOM,null);
    }

    public void insertNom(String nom) {
        editor.putString(NOM,nom);
        editor.commit();
    }

    public String getNom(){
        return prefs.getString(NOM,null);
    }

    public void insertBirthdate(String birthdate) {
        editor.putString(BIRTHDATE,birthdate);
        editor.commit();
    }

    public String getBirthdate(){
        return prefs.getString(BIRTHDATE,null);
    }

    public void insertPhoneNb(String phone) {
        editor.putString(PHONE,phone);
        editor.commit();
    }

    public String getPhoneNb(){
        return prefs.getString(PHONE,null);
    }

    public void insertAddress(String address) {
        editor.putString(ADDRESS,address);
        editor.commit();
    }

    public String getAddress(){
        return prefs.getString(ADDRESS,null);
    }

    public void insertEmail(String email) {
        editor.putString(EMAIL,email);
        editor.commit();
    }

    public String getEmail(){
        return prefs.getString(EMAIL,null);
    }

    public void insertPassword(String password) {
        editor.putString(PASSWORD,password);
        editor.commit();
    }

    public String getPassword(){
        return prefs.getString(PASSWORD,null);
    }

    @Override
    public String toString() {
        return "SessionManager{" +
                "ID = "+ID+
                '}';
    }
}
