package panier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.m1projetachitecture.R;

import java.util.List;

import navigation.MainApp;
import model.ShopItem;
import model.wrapper.ShoppingCartItemWrapper;

// Adapter permettant de personnaliser la liste des boissons dans le panier
public class ShoppingCartAdapter extends BaseAdapter {
    public Context mContext;
    private List<ShopItem> mShoppingCartList;
    private LayoutInflater mInflater;

    public ShoppingCartAdapter(Context context, List<ShopItem> shopList) {
        mContext = context;
        mShoppingCartList = shopList;
        this.mInflater = LayoutInflater.from(context);
    }

    public ShoppingCartAdapter getAdapter() {
        return this;
    }

    @Override
    public int getCount() {
        return mShoppingCartList.size();
    }

    @Override
    public ShopItem getItem(int position) {
        return mShoppingCartList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //On instancie la vue et on tague la ligne pour pouvoir récupérer les boutons qui gèrent la quantité.
        convertView = mInflater.inflate(R.layout.adapter_cart_item, null);
        ShoppingCartItemWrapper wrapper = new ShoppingCartItemWrapper(convertView);

        ShopItem currentItem = (ShopItem) getItem(position);
        String itemName = currentItem.getName();
        double itemPrice = currentItem.getPrice();
        String itemDescription = currentItem.getDescription();
        int itemAmount = currentItem.getAmountSelected();


        //On ajuste ce qui est affiché aux valeurs des items de la liste
        TextView textViewItemName = convertView.findViewById(R.id.shoppingCartItem_textview_drinkName);
        textViewItemName.setText(itemName);

        TextView textViewItemPrice = convertView.findViewById(R.id.shoppingCartItem_textview_drinkPrice);
        textViewItemPrice.setText(itemPrice + "€");

        TextView textViewItemDescription = convertView.findViewById(R.id.shoppingCartItem_textview_drinkDescription);
        textViewItemDescription.setText(itemDescription);

        TextView textViewItemAmount = convertView.findViewById(R.id.shoppingCartItem_textview_amount);
        textViewItemAmount.setText(Integer.toString(itemAmount));

        //On récupère le bouton de suppression et on y associe l'écouteur correspondant
        wrapper.getDeleteButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               MainApp.mShoppingCartFragment.updateCart(currentItem, false, getAdapter());
            }
        });

        return convertView;
    }

}