package panier;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

// Gestion de Braintree
public class PaiementRequest {

    public static final String API_CHECK_OUT = "http://10.0.2.2/braintree/Checkout.php";

    private Context context;
    private RequestQueue queue;

    public PaiementRequest(Context context, RequestQueue queue) {
        this.context = context;
        this.queue = queue;
    }

    public void sendPayments(final String amount, final String nonce, PaymentsCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API_CHECK_OUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.toString().contains("Successful"))
                            callback.onSuccess(true);
                        else {
                            callback.onError("Erreur lors du paiement");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError("EDMT_ERROR");
            }
        })
        {
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("amount", amount);
                map.put("nonce", nonce);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type" , "application/x-www-form-urlencoded");
                return map;
            }
        };
        queue.add(stringRequest);

    }

    public interface PaymentsCallback{
        void onSuccess(boolean success);
        void onError(String message);
    }


}
