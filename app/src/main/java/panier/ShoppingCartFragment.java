package panier;

import static navigation.MainApp.menuItem;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.RequestQueue;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.example.m1projetachitecture.R;

import bdd.DatabaseRequest;
import bdd.VolleySingleton;
import livraison.OrderConfirmedFragment;
import cz.msebera.android.httpclient.Header;
import dmax.dialog.SpotsDialog;
import model.SessionManager;
import model.ShopItem;
import navigation.MainApp;
import navigation.MapFragment;
import navigation.MenuFragment;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;
import java.util.Calendar;

public class ShoppingCartFragment extends Fragment {

    public static final String API_TOKEN_URL = "http://10.0.2.2/braintree/main.php";

    Button mConfirmOrder;
    Button mBackToMenuButton;
    ListView mShoppingCartItemsListView;

    TextView mEmptyCartTextView;
    TextView mTotalTextView;
    TextView mTotalPriceTextView;
    TextView mShopAddress;
    TextView mShopName;

    SessionManager session;

    boolean mIsViewCreated;

    View mView;

    ShoppingCartAdapter shoppingCartAdapter;

    TextView amountEdt;
    TextView paymentTV;

    String Token;

    //POUR LE PAIEMENT--------------
    private RequestQueue queue;
    private PaiementRequest request;
    private DatabaseRequest requestBDD;
    //-------------------------

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        session = new SessionManager(this.getContext());
        mView = inflater.inflate(R.layout.fragment_shopping_cart, container, false);
        mBackToMenuButton = mView.findViewById(R.id.shoppingCart_button_backtomenu);
        mConfirmOrder = mView.findViewById(R.id.shoppingCart_button_confirmOrder);

        mTotalTextView = mView.findViewById(R.id.shoppingCart_textview_total);
        mTotalPriceTextView = mView.findViewById(R.id.shoppingCart_textview_totalPrice);
        mShopAddress = mView.findViewById(R.id.shoppingCart_textview_shopAddress);
        mShopName = mView.findViewById(R.id.shoppingCart_textview_shopName);
        mEmptyCartTextView = mView.findViewById(R.id.shoppingCart_textview_emptyCart);
        if (mEmptyCartTextView != null) Log.d("différent de null", "dans le panier");
        mShoppingCartItemsListView = mView.findViewById(R.id.shoppingCart_listview);

        shoppingCartAdapter = new ShoppingCartAdapter(this.getContext(), MainApp.mCartItems);
        mShoppingCartItemsListView.setAdapter(shoppingCartAdapter);

        mBackToMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fr = getParentFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                fr.replace(R.id.fragment_container, new MenuFragment()).commit();
            }
        });
        setViewCreated(true);

        //Permet de ne pas réinstancier à chaque fois la RequestQueue et d'en avoir qu'une seule pour toute l'application
        queue = VolleySingleton.getInstance(this.getContext()).getRequestQueue();
        request = new PaiementRequest(this.getContext(),queue);
        requestBDD = new DatabaseRequest(this.getContext(),queue);

        mConfirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //On a cliqué sur le bouton PAYER. On appelle donc notre API qui va gérer le paiement
                final android.app.AlertDialog waitingDialog = new SpotsDialog.Builder().setContext(getContext()).setMessage("Please wait...").build();       //(ShoppingCartFragment.this, "Please wait...", 1, false);
                waitingDialog.show();

                AsyncHttpClient client = new AsyncHttpClient();
                client.get(API_TOKEN_URL, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        waitingDialog.dismiss();
                        Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    //Récupération du token pour l'API réussite : on démarre le paiement
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        waitingDialog.dismiss();
                        Token = responseString;
                        Log.d("APP", Token);
                        DropInRequest dropInRequest = new DropInRequest().clientToken(Token);
                        Log.d("APP", dropInRequest.getIntent(getContext()).toString());
                        startActivityForResult(dropInRequest.getIntent(getContext()), 1111);
                    }
                });
            }
        });

        updateCartStatus();
        updateTotalPrice();

        amountEdt = mTotalPriceTextView;
        paymentTV = mView.findViewById(R.id.shoppingCart_textview_totalPrice);

        return mView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String amount = amountEdt.getText().toString();
        amount = amount.substring(0, amount.length() - 2);
        if(data != null) {
            DropInResult resultat = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
            PaymentMethodNonce nonce = resultat.getPaymentMethodNonce();
            String strNonce = nonce.getNonce();

            request.sendPayments(amount, strNonce, new PaiementRequest.PaymentsCallback() {
                @Override
                public void onSuccess(boolean success) {
                    //On ajoute la commande à la BDD

                    for(int i=0 ; i < MainApp.mCartItems.size() ; i++)
                    {
                        try {
                           String user_id = session.getId().toString();
                           String article_id = ""+MainApp.mCartItems.get(i).getId();
                           String amount = ""+MainApp.mCartItems.get(i).getAmountSelected();
                           String shop_id = ""+MainApp.mCartItems.get(i).getShop().getId();
                           String delivry_address = MapFragment.getUserAddress(getContext());
                           String delivry_date = Calendar.getInstance().getTime().toString();
                           Log.d("APP", user_id);
                           Log.d("APP", article_id);
                           Log.d("APP", shop_id);
                           Log.d("APP", delivry_address);
                           Log.d("APP", delivry_date);
                           requestBDD.addOrderToBDD(user_id, article_id, amount, shop_id, delivry_address, delivry_date, new DatabaseRequest.OrderCallback() {
                               @Override
                               public void onSuccess(String message) {
                                    Log.d("APP",message);
                               }

                               @Override
                               public void onError(String message) {
                                   Log.d("APP","ERREUR : " + message);
                               }
                           });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    //On peut passer à la simulation de la livraison
                    FragmentTransaction fr = getParentFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                    fr.replace(R.id.fragment_container, new OrderConfirmedFragment()).commit();
                    //On vide la carte
                    for (int i = 0; i < MainApp.mCartItems.size(); i++) {
                        updateCart(MainApp.mCartItems.get(i), false, shoppingCartAdapter);
                    }
                    updateCartStatus();
                    updateTotalPrice();

                }

                @Override
                public void onError(String message) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public boolean isViewCreated() {
        return mIsViewCreated;
    }

    public void setViewCreated(boolean viewCreated) {
        mIsViewCreated = viewCreated;
    }

    public void updateCart(ShopItem item, boolean add, ShoppingCartAdapter adapter) {

        View actionView = menuItem.getActionView();
        TextView cartBadgeTextView = actionView.findViewById(R.id.cart_textview_badge);

        int amount = Integer.parseInt(cartBadgeTextView.getText().toString());
        if (add) {
            //On ajoute le(s) produit(s) au panier.
            MainApp.mCartItems.add(item);
            cartBadgeTextView.setText(Integer.toString(amount + item.getAmountSelected()));
        }
        else {
            //On supprime le(s) produit(s) du panier.
            if (Integer.parseInt(cartBadgeTextView.getText().toString()) > 0) {
                MainApp.mCartItems.remove(item);
                adapter.notifyDataSetChanged();
                cartBadgeTextView.setText(Integer.toString(amount - item.getAmountSelected()));
            }
        }

        updateCartStatus();
        updateTotalPrice();
    }

    public void updateTotalPrice() {
        if (MainApp.mCartItems.size() > 0) {
            double totalPrice = 0;
            for (int i = 0; i < MainApp.mCartItems.size(); i++) {
                totalPrice += MainApp.mCartItems.get(i).getPrice()*MainApp.mCartItems.get(i).getAmountSelected();
            }

            if (isViewCreated()) mTotalPriceTextView.setText(Double.toString(totalPrice) + " €");
        }
    }

    public  void updateCartStatus() {
        if (isViewCreated()) {
            //Si le panier n'est pas vide, on cache le texte qui indique à l'utilisateur qu'il n'a rien ajouté.
            if (MainApp.mCartItems.size() > 0) {
                mShopAddress.setText(MainApp.mCartItems.get(0).getShop().getAddress());
                mShopName.setText(MainApp.mCartItems.get(0).getShop().getName());
                if (mEmptyCartTextView.getVisibility() == View.VISIBLE) mEmptyCartTextView.setVisibility(View.INVISIBLE);
            } else {
                //Si c'est vide
                mShopName.setVisibility(View.INVISIBLE);
                mShopAddress.setVisibility(View.INVISIBLE);
                mTotalTextView.setVisibility(View.INVISIBLE);
                mTotalPriceTextView.setVisibility(View.INVISIBLE);
                mConfirmOrder.setVisibility(View.INVISIBLE);
                mEmptyCartTextView.setVisibility(View.VISIBLE);
            }
        }
    }




}
