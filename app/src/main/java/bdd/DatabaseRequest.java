package bdd;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.Shop;
import model.ShopItem;
import model.User;

// L'API Gateway contenant les fonctions d'appels aux services internes et à la base de données
public class DatabaseRequest {
    
    private Context context;
    private RequestQueue queue;

    public static String ip = "192.168.56.1";

    public DatabaseRequest(Context context, RequestQueue queue) {
        this.context = context;
        this.queue = queue;
    }

    //--------------------------------REGISTER--------------------------------------------------------
    public void register(final String prenom, final String nom, final String birthdate, final String phone, final String address,  final String mail, final String password, final String passwordConfirm,RegisterCallback callback)
    {
        //On construit l'appel au serveur
       String url = "http://"+this.ip+"/BDD_ARCHI/register.php";

       StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Map<String, String> errors = new HashMap<>();
                try {
                    JSONObject json = new JSONObject(response);
                    boolean error = json.getBoolean("error");
                    if(!error)
                    {
                        int id = Integer.parseInt(json.getString("id"));
                        User user = new User(id,prenom,nom,birthdate,phone,address,mail,password);
                        //L'inscription s'est bien déroulée

                        callback.onSuccess("Vous êtes bien inscrit",user);

                    }
                    else
                    {
                        //Une erreur s'est produite sur le serveur du au mauvais remplissage du formulaire.
                        // On la récupère et on l'affiche.
                        JSONObject message = json.getJSONObject("message");

                        if(message.has("prenom")){
                            errors.put("prenom",message.getString("prenom"));
                        }
                        if(message.has("nom")){
                            errors.put("nom",message.getString("nom"));
                        }
                        if(message.has("birthdate")){
                            errors.put("birthdate",message.getString("birthdate"));
                        }
                        if(message.has("phone")){
                            errors.put("phone",message.getString("phone"));
                        }
                        if(message.has("address")){
                            errors.put("address",message.getString("address"));
                        }
                        if(message.has("mail")){
                            errors.put("mail",message.getString("mail"));
                        }
                        if(message.has("password")){
                            errors.put("password",message.getString("password"));
                        }
                        if(message.has("passwordConfirm")){
                            errors.put("passwordConfirm",message.getString("passwordConfirm"));
                        }

                        callback.inputError(errors);

                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
           // Une erreur Volley a eu lieu.
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error instanceof NetworkError) {
                    callback.onError("Impossible de se connecter");
                }
                else if(error instanceof VolleyError) {
                    callback.onError("Une erreur s'est produite");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //On récupère les paramètres à passer au serveur.

                Map<String, String> map = new HashMap<>();
                map.put("prenom", prenom);
                map.put("nom",nom);
                map.put("birthdate",birthdate);
                map.put("phone",phone);
                map.put("address",address);
                map.put("mail",mail);
                map.put("password",password);
                map.put("passwordConfirm", passwordConfirm);

                return map;
            }
        };

        queue.add(request);
    }

    public interface RegisterCallback {
        void onSuccess(String message, User user);
        void inputError(Map<String,String> errors); //Erreur dans l'input
        void onError(String message); //Erreur de connexion etc
    }

    //---------------------------------LOGIN-----------------------------------------------------------
    public void login(final String mail, final String password, LoginCallback callback)
    {
        String url = "http://"+this.ip+"/BDD_ARCHI/login.php";

        //On construit l'appel au serveur
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    boolean error = json.getBoolean("error");
                    if(!error){
                        int id = Integer.parseInt(json.getString("id"));
                        String prenom = json.getString("prenom");
                        String nom = json.getString("nom");
                        String birthdate = json.getString("birthdate");
                        String phone = json.getString("phone");
                        String address = json.getString("address");
                        String mail = json.getString("mail");
                        String password = json.getString("password");
                        User user = new User(id, prenom,nom,birthdate,phone,address,mail,password);
                        callback.onSuccess("Vous êtes connecté", user);
                    }
                    else{
                        //Une erreur s'est produite sur le serveur.
                        // On la récupère et on l'affiche.
                        callback.onError(json.getString("message"));
                    }

                } catch (JSONException e) {
                    callback.onError("Une erreur s'est produite");
                    e.printStackTrace();
                }
              
            }
        }, new Response.ErrorListener() {
            // Une erreur Volley a eu lieu.
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error instanceof NetworkError) {
                    callback.onError("Impossible de se connecter");
                }
                else if(error instanceof VolleyError) {
                    callback.onError("Une erreur s'est produite");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("mail",mail);
                map.put("password",password);
                return map;
            }
        };

        queue.add(request);
    }

    public interface LoginCallback{
        void onSuccess(String message, User user);
        void onError(String message);
    }

    //---------------------------------SHOP-----------------------------------------------------------
    public void getShopFromDB(ShopCallback callback)
    {
        //On construit l'appel au serveur
        String url = "http://"+this.ip+"/BDD_ARCHI/shop.php";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray(response);
                    ArrayList<Shop> shopList = new ArrayList<>();
                    for(int i = 0; i<json.length() ;i++)
                    {
                        int id = Integer.parseInt(json.getJSONObject(i).getString("shop_id"));
                        String name = json.getJSONObject(i).getString("name");
                        String address = json.getJSONObject(i).getString("address");
                        String description = json.getJSONObject(i).getString("description");
                        double lat = Double.parseDouble(json.getJSONObject(i).getString("lat"));
                        double lon = Double.parseDouble(json.getJSONObject(i).getString("lon"));

                        shopList.add(new Shop(id,name,address,description, lat, lon));
                    }
                    callback.onSuccess(shopList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Une erreur Volley a eu lieu.
                if(error instanceof NetworkError) {
                    callback.onError("Impossible de se connecter");
                }
                else if(error instanceof VolleyError) {
                    callback.onError("Une erreur s'est produite");
                }
            }
        });
        queue.add(request);
    }

    public interface ShopCallback{
        void onSuccess(ArrayList<Shop> liste);
        void onError(String message);
    }

//---------------------------------SHOP ITEMS-----------------------------------------------------------
    public void getItemsFromDB(final int id, Shop shop, ItemCallback callback)
    {
        //On construit l'appel au serveur
        String url = "http://"+this.ip+"/BDD_ARCHI/items.php";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray(response);
                    ArrayList<ShopItem> itemList = new ArrayList<>();
                    for(int i = 0; i<json.length() ;i++)
                    {
                        int id = Integer.parseInt(json.getJSONObject(i).getString("article_id"));
                        String name = json.getJSONObject(i).getString("nom");
                        double price = Double.parseDouble(json.getJSONObject(i).getString("price"));
                        String description = json.getJSONObject(i).getString("description");
                        itemList.add(new ShopItem(id,name,price,description,shop));
                    }
                    callback.onSuccess(itemList);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Une erreur Volley a eu lieu.
                if(error instanceof NetworkError) {
                    callback.onError("Impossible de se connecter");
                }
                else if(error instanceof VolleyError) {
                    callback.onError("Une erreur s'est produite");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("id", ""+id);

                return map;
            }
        };
        queue.add(request);
    }

    public interface ItemCallback{
        void onSuccess(ArrayList<ShopItem> liste);
        void onError(String message);
    }

    //---------------------------------ORDER-----------------------------------------------------------
    public void addOrderToBDD(String user_id, String article_id, String amount, String shop_id, String delivry_address, String delivry_date, OrderCallback callback)
    {
        String url = "http://"+this.ip+"/BDD_ARCHI/order.php";
        //On construit l'appel au serveur
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    boolean error = json.getBoolean("error");

                     if(!error)
                    {
                       callback.onSuccess("Article de la commande ajouté à la BDD");
                    }
                    else
                    {
                        //Une erreur s'est produite sur le serveur du au mauvais remplissage du formulaire.
                        // On la récupère et on l'affiche.
                       callback.onError(json.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Une erreur Volley a eu lieu.
                if(error instanceof NetworkError) {
                    callback.onError("Impossible de se connecter");
                }
                else if(error instanceof VolleyError) {
                    callback.onError("Une erreur s'est produite");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id",user_id);
                map.put("article_id",article_id);
                map.put("amount", amount);
                map.put("shop_id",shop_id);
                map.put("delivry_address",delivry_address);
                map.put("delivry_date",delivry_date);
                return map;
            }
        };
        queue.add(request);
    }

    public interface OrderCallback{
        void onSuccess(String message);
        void onError(String message);
    }
}

