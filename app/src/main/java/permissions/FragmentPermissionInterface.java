package permissions;

public interface FragmentPermissionInterface {

    public void onGranted(boolean isGranted);
}
