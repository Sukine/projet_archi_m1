package permissions;

import android.Manifest;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.FragmentActivity;

public class FragmentPermissionHelper {

    public void requestLocationPermission(FragmentActivity frag, FragmentPermissionInterface fragInterface) {
        ActivityResultLauncher<String> requestLauncher = frag.registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            fragInterface.onGranted(isGranted);
        });
        requestLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
        requestLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION);
    }

}