package livraison;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import bdd.DatabaseRequest;

public class DeliveryRequest {
    private Context context;
    private RequestQueue queue;

    public DeliveryRequest(Context context, RequestQueue queue) {
        this.context = context;
        this.queue = queue;
    }

    public void getNextOrderNumber(DeliveryCallback callback) {
        String url = "http://"+ DatabaseRequest.ip+"/BDD_ARCHI/order_number.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                            callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError("Impossible d'obtenir le numéro de commande.");
                Log.d("test", "voici l'erreur = " + error.toString());
            }
        });
        queue.add(stringRequest);
    }

    public void getOrderArrival(DeliveryCallback callback) {
        String url = "http://"+ DatabaseRequest.ip+"/BDD_ARCHI/order_arrival.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                            callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError("L'heure de livraison n'a pas pu être récupérée.");
            }
        });
        queue.add(stringRequest);
    }

    public void getOrderState(final String status, DeliveryCallback callback) {
        String url = "http://" + DatabaseRequest.ip + "/BDD_ARCHI/order_state.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError("Impossible d'obtenir l'état de la commande.");
            }
        }) {
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("status", status);
                return map;
            }
        };
        queue.add(stringRequest);
    }



    public interface DeliveryCallback{
        void onSuccess(String result);
        void onError(String message);
    }
}
