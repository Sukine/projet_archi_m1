package livraison;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.example.m1projetachitecture.R;

import java.io.IOException;

import bdd.VolleySingleton;
import navigation.MapFragment;

//Fragment gérant la livraison des boissons.
public class OrderConfirmedFragment extends Fragment {

    TextView mOrderAddress;
    TextView mOrderArrival;
    TextView mOrderNumber;
    TextView mOrderProgress;
    ProgressBar mProgressBar;
    private int progressBarStatus;
    private Handler progressBarHandler;
    DeliveryRequest deliveryRequest;
    RequestQueue queue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_order_success, container, false);

        queue = VolleySingleton.getInstance(this.getContext()).getRequestQueue();
        deliveryRequest = new DeliveryRequest(this.getContext(), queue);

        // On récupère les éléments de la vue
        mOrderNumber = mView.findViewById(R.id.orderSuccess_textview_orderNumber);
        mOrderArrival = mView.findViewById(R.id.orderSuccess_textView_orderArrival);
        mOrderAddress = mView.findViewById(R.id.orderSuccess_textView_orderAddress);
        mOrderProgress = mView.findViewById(R.id.orderSuccess_textView_orderProgress);
        mProgressBar = (ProgressBar) mView.findViewById(R.id.progressBar);

        Log.d("APP", "on est ici");
        deliveryRequest.getNextOrderNumber(new DeliveryRequest.DeliveryCallback() {
            @Override
            public void onSuccess(String result) {
                mOrderNumber.setText(Integer.toString(Integer.parseInt(result)));
            }

            @Override
            public void onError(String message) {
                Log.d("APP", "erreur = " + message);
            }
        });

        deliveryRequest.getOrderArrival(new DeliveryRequest.DeliveryCallback() {
            @Override
            public void onSuccess(String result) {
                mOrderArrival.setText(result);
            }

            @Override
            public void onError(String message) {
                Log.d("APP", "erreur = " + message);
            }
        });

        //On définit l'adresse de livraison
        try {
            mOrderAddress.setText(MapFragment.getUserAddress(this.getContext()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        progressBarHandler = new Handler();
        progressBarStatus = 0;

        // Gestion de la barre d'état de la livraison
        new Thread(new Runnable() {
            public void run() {
                while (progressBarStatus <= 240) {
                    progressBarHandler.post(new Runnable() {
                        public void run() {
                            mProgressBar.setProgress(progressBarStatus);
                            if ((progressBarStatus-1)%80 ==0) {
                                deliveryRequest.getOrderState(Integer.toString(progressBarStatus), new DeliveryRequest.DeliveryCallback() {
                                    @Override
                                    public void onSuccess(String result) {
                                        mOrderProgress.setText(result);
                                    }

                                    @Override
                                    public void onError(String message) {
                                        Log.d("APP", "erreur = " + message);
                                    }
                                });

                            }
                        }
                    });
                    progressBarStatus += 1;
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        return mView;
    }
}
