package navigation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.m1projetachitecture.R;

import java.util.List;

import model.Shop;

// Adapter permettant de personnaliser la liste des commerces
public class ShopAdapter extends BaseAdapter {

    private Context mContext;
    private List<Shop> mShopList;
    private LayoutInflater mInflater;

    public ShopAdapter(Context context, List<Shop> shopList) {
        mContext = context;
        mShopList = shopList;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() { return mShopList.size(); }

    @Override
    public Shop getItem(int position) {
        return mShopList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //On instancie la vue et on tague la ligne pour pouvoir récupérer les boutons qui gèrent la quantité.
        convertView = mInflater.inflate(R.layout.adapter_shop_instance, null);

        Shop currentItem = getItem(position);
        String shopName = currentItem.getName();
        String shopDescription = currentItem.getDescription();

        //On affiche le nom et la description de chaque commerce
        TextView shopNameView = convertView.findViewById(R.id.shopInstance_textView_shopName);
        shopNameView.setText(shopName);

        TextView shopDescriptionView = convertView.findViewById(R.id.shopInstance_textView_shopDescription);
        shopDescriptionView.setText(shopDescription);

        return convertView;
    }
}
