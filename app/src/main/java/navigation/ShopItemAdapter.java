package navigation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.m1projetachitecture.R;

import java.util.ArrayList;
import java.util.List;

import navigation.MainApp;
import model.ShopItem;
import model.wrapper.ShopItemWrapper;

// Adapter permettant de personnaliser la liste des boissons d'un commerce
public class ShopItemAdapter extends BaseAdapter {

    private Context mContext;
    private List<ShopItem> mShopItemList;
    private LayoutInflater mInflater;
    private List<Button> mButtonsLess;
    private List<Button> mButtonsMore;
    private List<Button> mAddToCartButtons;

    public ShopItemAdapter(Context context, List<ShopItem> shopItemList) {
        mContext = context;
        mShopItemList = shopItemList;
        mInflater = LayoutInflater.from(context);
        mButtonsLess = new ArrayList<Button>();
        mButtonsMore = new ArrayList<Button>();
        mAddToCartButtons = new ArrayList<Button>();
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public List<ShopItem> getShopItemsList() {
        return mShopItemList;
    }

    public void setShopItemsList(List<ShopItem> shopItemList) {
        mShopItemList = shopItemList;
    }

    @Override
    public String toString() {
        return "ShopItemAdapter{" +
                "mContext=" + mContext +
                ", mShopItemsList=" + mShopItemList +
                '}';
    }

    @Override
    public int getCount() {
        return mShopItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mShopItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //On instancie la vue et on tague la ligne pour pouvoir récupérer les boutons qui gèrent la quantité.
        convertView = mInflater.inflate(R.layout.adapter_shop_item, null);
        ShopItemWrapper wrapper = new ShopItemWrapper(convertView);
        convertView.setTag(wrapper);

        ShopItem currentItem = (ShopItem) getItem(position);
        String itemName = currentItem.getName();
        double itemPrice = currentItem.getPrice();
        int itemAmount = currentItem.getAmountSelected();
        String itemDescription = currentItem.getDescription();


        //On ajuste ce qui est affiché aux valeurs des items de la liste
        TextView textViewItemName = convertView.findViewById(R.id.adapterShopItem_textview_drinkName);
        textViewItemName.setText(itemName);

        TextView textViewItemPrice = convertView.findViewById(R.id.adapterShopItem_textview_drinkPrice);
        textViewItemPrice.setText(itemPrice + "€");

        TextView textViewItemDescription = convertView.findViewById(R.id.adapterShopItem_textview_drinkDescription);
        textViewItemDescription.setText(itemDescription);

        //On récupère les boutons et on leur associe les écouteurs correspondants
        wrapper.getButtonMore().setOnClickListener(v -> {
            currentItem.setAmountSelected(currentItem.getAmountSelected()+1);
            TextView textViewItemAmount = wrapper.getAmountSelected();
            textViewItemAmount.setText(Integer.toString(currentItem.getAmountSelected()));
        });

        wrapper.getButtonLess().setOnClickListener(v -> {
            if (currentItem.getAmountSelected() > 0) {
                currentItem.setAmountSelected(currentItem.getAmountSelected() - 1);
                TextView textViewItemAmount = wrapper.getAmountSelected();
                textViewItemAmount.setText(Integer.toString(currentItem.getAmountSelected()));
            }
        });

        wrapper.getButtonAddToCart().setOnClickListener(v -> {
            if(currentItem.getAmountSelected() > 0)
                MainApp.mShoppingCartFragment.updateCart(currentItem, true, null);
        });

        return convertView;
    }
}
