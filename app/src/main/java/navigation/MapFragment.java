package navigation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.m1projetachitecture.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import model.Shop;
import permissions.FragmentPermissionHelper;
import permissions.FragmentPermissionInterface;

// Gère la carte Google dans le composant de navigation
public class MapFragment extends Fragment {

    public static Location userLocation;

    SupportMapFragment supportMapFragment;
    FusedLocationProviderClient client;

    private MarkerOptions mMarkerOptions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        //On récupère la carte de la vue.
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment_google_map);

        client = LocationServices.getFusedLocationProviderClient(this.getContext());

        //On demande à l'utilisateur l'autorisation d'utiliser sa localisation
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation();

        } else {
            new FragmentPermissionHelper().requestLocationPermission(getActivity(), new FragmentPermissionInterface() {
                @Override
                public void onGranted(boolean isGranted){
                    if (isGranted) {
                        getCurrentLocation();

                    }
                }
            });
        }

        return view;
    }

    //Récupère la localisation de l'utilisateur et on place la camera de la carte à cet endroit.
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    userLocation = task.getResult();

                    if (userLocation != null) {
                        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(@NonNull GoogleMap googleMap) {
                                //On positionne le marqueur sur la carte et on zoom vers ce point
                                LatLng latLng = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
                                googleMap.addMarker(markerOptions);
                            }
                        });

                    }
                }
            });

        }

    }

    public void getShopMarkers(List<Shop> shopList) {
        for (int i = 0; i < shopList.size(); i++) {
            LatLng latLng = new LatLng(shopList.get(i).getLat(), shopList.get(i).getLon());
            MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(shopList.get(i).getName());
            supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(@NonNull GoogleMap googleMap) {
                    googleMap.addMarker(markerOptions);
                }
            });
        }
    }


    public static String getUserAddress(Context context) throws IOException {
        List<Address> addresses;

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        addresses = geocoder.getFromLocation(MapFragment.userLocation.getLatitude(), MapFragment.userLocation.getLongitude(), 1);

        return addresses.get(0).getAddressLine(0);
    }

}