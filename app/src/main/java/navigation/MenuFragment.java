package navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.RequestQueue;
import com.example.m1projetachitecture.R;

import java.util.ArrayList;
import java.util.List;

import bdd.DatabaseRequest;
import bdd.VolleySingleton;
import model.Shop;

// Fragment gérant la liste des commerces
public class MenuFragment extends Fragment {

    ListView mShopListView;
    List<Shop> mShopList;

    Context myContext;

    MapFragment mMapFragment;

    private RequestQueue queue;
    private DatabaseRequest request;

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int nbGridRow, nbGridCol;

        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        mMapFragment = new MapFragment();
        getParentFragmentManager().beginTransaction().replace(R.id.mainMenu_frameLayout_map, mMapFragment).commit();


        //Permet de ne pas réinstancier à chaque fois la RequestQueue et d'en avoir qu'une seule pour toute l'application
        queue = VolleySingleton.getInstance(this.getContext()).getRequestQueue();
        request = new DatabaseRequest(this.getContext(),queue);

        myContext = this.getContext();

        mShopList = new ArrayList<>();

        //Récupération des commerces de la base de données.
        request.getShopFromDB(new DatabaseRequest.ShopCallback() {
            @Override
            public void onSuccess(ArrayList<Shop> liste) {
                mShopList = liste;
                mMapFragment.getShopMarkers(mShopList);
                mShopListView = view.findViewById(R.id.mainMenu_listview);
                mShopListView.setAdapter(new ShopAdapter(myContext, mShopList));
                //A chaque commerce, on associe le fragment qui correspond à la carte des boissons.
                mShopListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Shop currentShop = (Shop)parent.getAdapter().getItem(position);

                        if (MainApp.mCartItems.size() == 0 || currentShop.equals(MainApp.mCartItems.get(0).getShop())) {

                            Bundle bundle = new Bundle();
                            bundle.putParcelable("shop", currentShop);

                            ShopFragment shopFragment = new ShopFragment();
                            shopFragment.setArguments(bundle);

                            FragmentTransaction fr = getParentFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                            fr.replace(R.id.fragment_container, shopFragment).commit();

                        }
                        else {
                            Log.d("test", "on est ici");
                            //L'utilisateur essaie de commander depuis un autre commerce
                            Toast.makeText(getActivity(),"Vous devez d'abord vider votre panier.",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

            @Override
            public void onError(String message) {
                Log.d("ERROR SHOP",message);
            }
        });



        return view;
    }

}