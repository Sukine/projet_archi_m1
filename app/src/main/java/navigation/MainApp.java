package navigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m1projetachitecture.R;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import authentification.LogIn;
import panier.ShoppingCartFragment;
import model.SessionManager;
import model.ShopItem;
import model.User;

// Application principale
public class MainApp extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static List<ShopItem> mCartItems;
    public static MenuItem menuItem;
    public static ShoppingCartFragment mShoppingCartFragment;

    private DrawerLayout drawer;

    private SessionManager sessionManager;

    private User user;

    TextView drawerUserName;
    TextView drawerUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_app);

        //On charge la barre de navigation en haut de l'écran
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if(intent.hasExtra("CONNEXION")){
            Toast.makeText(this,intent.getStringExtra("CONNEXION"), Toast.LENGTH_SHORT).show();
        }

        sessionManager = new SessionManager(this);
        if(sessionManager.isLogged())
        {
            int id = Integer.parseInt(sessionManager.getId());
            String prenom = sessionManager.getPrenom();
            String nom = sessionManager.getNom();
            String birthdate = sessionManager.getBirthdate();
            String phone = sessionManager.getPhoneNb();
            String address = sessionManager.getAddress();
            String email = sessionManager.getEmail();
            String password = sessionManager.getPassword();

            //Toute les informations concernant l'utilisateur sont stockées dans user
            user = new User(id,prenom,nom,birthdate,phone,address,email,password);

        }

        mCartItems = new ArrayList<ShopItem>();

        //On charge le tiroir de navigation.
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //On ajoute l'action de basculement entre le menu et le tiroir.
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawer,toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MenuFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_menu);
        }

        mShoppingCartFragment = new ShoppingCartFragment();

        //On récupère les champs de la vue
        View headerView = navigationView.getHeaderView(0);
        drawerUserName = headerView.findViewById(R.id.drawer_textView_userName);
        drawerUserEmail = headerView.findViewById(R.id.drawer_textView_userEmail);

        drawerUserName.setText(user.getfirstName() + " " + user.getmLastName());
        drawerUserEmail.setText(user.getmEmail());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.topbar,menu);

        //On rend le bouton du panier cliquable
        menuItem = menu.findItem(R.id.topbar_item_cart);
        View actionView = menuItem.getActionView();

        TextView cartBadgeTextView = actionView.findViewById(R.id.cart_textview_badge);

        cartBadgeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        FragmentTransaction fr = getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fr.replace(R.id.fragment_container, mShoppingCartFragment).commit();
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //En fonction de l'option sélectionnée, on charge le fragment correspondant.
        switch (item.getItemId()) {
            case R.id.nav_logout :
                sessionManager.logout();
                Intent logOutIntent = new Intent(getApplicationContext(), LogIn.class);
                startActivity(logOutIntent);
                finish();

                break;
            case R.id.nav_menu:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MenuFragment()).commit();
                break;

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    //Permet à l'utilisateur de fermer le tiroir lorsqu'il clique sur le bouton retour de son téléphone.
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))  {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }
}