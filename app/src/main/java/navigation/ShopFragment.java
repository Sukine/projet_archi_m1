package navigation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.example.m1projetachitecture.R;

import java.util.ArrayList;
import java.util.List;

import bdd.DatabaseRequest;
import bdd.VolleySingleton;
import model.Shop;
import model.ShopItem;


public class ShopFragment extends Fragment {

    Button mBackToMenuButton;
    ListView mShopItemsListView;
    List<ShopItem> mItemsList;

    //POUR LA BDD--------------
    private RequestQueue queue;
    private DatabaseRequest request;
    //-------------------------

    Context myContext;

    TextView mShopName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        mBackToMenuButton = view.findViewById(R.id.shop_button_backtomenu);
        mShopName = view.findViewById(R.id.shop_textView_shopName);

        //Permet de ne pas réinstancier à chaque fois la RequestQueue et d'en avoir qu'une seule pour toute l'application
        queue = VolleySingleton.getInstance(this.getContext()).getRequestQueue();
        request = new DatabaseRequest(this.getContext(),queue);
        myContext = this.getContext();


        Bundle bundle = this.getArguments();
        Shop shop = bundle.getParcelable("shop");
        int idShop = shop.getId();
        mShopName.setText(shop.getName());
        mItemsList = new ArrayList<>();
        Log.d("APP", "on va faire la requete");
        request.getItemsFromDB(idShop, shop, new DatabaseRequest.ItemCallback() {
            @Override
            public void onSuccess(ArrayList<ShopItem> liste) {
                mItemsList = liste;
                mShopItemsListView = view.findViewById(R.id.shop_listview);
                mShopItemsListView.setAdapter(new ShopItemAdapter(myContext, mItemsList));
            }

            @Override
            public void onError(String message) {
                Log.d("ERROR ITEMS",message);
            }
        });

        mBackToMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fr = getParentFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                fr.replace(R.id.fragment_container, new MenuFragment()).commit();
            }
        });

        return view;

    }




}