<?php 

	//Connexion à la base de données
	$db = new PDO("mysql:host=localhost;dbname=projet_archi","root","");
	$results["error"] = false;
	$results["message"] = [];
	
	if(isset($_POST))
	{
		if(!empty($_POST['mail']) && !empty($_POST['password']))
		{
			$mail = $_POST['mail'];
			$password = $_POST['password'];
			
			$sql = $db->prepare("SELECT * FROM user WHERE email = :mail");
			$sql->execute([":mail" => $mail]);
			$row = $sql->fetch(PDO::FETCH_OBJ);
			if($row)
			{
				if(password_verify($password,$row->password))
				{
					$results['id'] = $row->user_id;
					$results['prenom'] = $row->first_name;
					$results['nom'] = $row->last_name;
					$results['birthdate'] = $row->birth_date;
					$results['phone'] = $row->phone_nb;
					$results['address'] = $row->address;
					$results['mail'] = $row->email;
					$results['password'] = $password;
				}
				else
				{
					$results['error'] = true;
					$results['message'] = "Pseudo ou mot de passe incorrect";
				}
			}
			else
			{
				$results['error'] = true;
				$results['message'] = "Pseudo ou mot de passe incorrect";
			}
		}
		else
		{
			$results['error'] = true;
			$results['message'] = "Veuillez remplir tous les champs";
		}
		echo json_encode($results);
	}

?>