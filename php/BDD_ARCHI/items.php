<?php

	//Connexion à la base de données
	$db = new PDO("mysql:host=localhost;dbname=projet_archi","root","");
	$results["error"] = false;
	$results["message"] = [];
	
	//$_POST['id'] = 3;

	if(isset($_POST))
	{
		if(!empty($_POST['id']))
		{
			$id = intval($_POST['id']);
			$sql = $db->prepare("SELECT * FROM article WHERE shop_id = :shop_id");
			$sql->execute([":shop_id" => $id]);
			$rows = $sql->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($rows);
		}
		else
		{
			$results['error'] = true;
			$results['message'] = "ID manquant";
		}
	}

?>