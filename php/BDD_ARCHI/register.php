<?php

//Connexion à la base de données
$db = new PDO("mysql:host=localhost;dbname=projet_archi","root","");
$results["error"] = false;
$results["message"] = [];
$results['id'] = null;

//DONNEES DE TEST
/*$_POST['prenom'] = "Sherlock";
$_POST['nom'] = "Holmes";
$_POST['birthdate'] = "29/01/1999";
$_POST['phone'] = "0606060606";
$_POST['address'] = "221B Baker Street";
$_POST['mail'] = "she.ho@gmail.com";
$_POST['password'] = "watson";
$_POST['passwordConfirm'] = "watson";*/


if(isset($_POST))
{		
	//Vérification du prénom
	if (empty($_POST['prenom']))
	{
		$results["error"] = true;
		$results["message"]["prenom"] = "Veuillez renseigner votre prénom";
	}
	//Vérification du nom
	if(empty($_POST['nom']))
	{
		$results["error"] = true;
		$results["message"]["nom"] = "Veuillez renseigner votre nom";
	}
	//Vérification de la date de naissance
	if(empty($_POST['birthdate']))
	{
		$results["error"] = true;
		$results["message"]["birthdate"] = "Veuillez renseigner votre date de naissance";
	}
	//Pas vide donc on vérifie le format jj/mm/aaaa
	list($jj,$mm,$aaaa) = explode('/',$_POST['birthdate']);
	if (!checkdate($mm,$jj,$aaaa)) 
	{
       $results["error"] = true;
	   $results["message"]["birthdate"] = "Format jj/mm/aaaa uniquement";
	}
	//Vérification du numéro de téléphone
	if(empty($_POST['phone']))
	{
		$results["error"] = true;
		$results["message"]["phone"] = "Veuillez renseigner votre numéro de téléphone";
	}
	else if(!preg_match("#[0-9]{10}#",$_POST['phone']))
	{
		$results["error"] = true;
		$results["message"]["phone"] = "Veuillez renseigner votre numéro de téléphone sur 10 digits";
	}
	//Vérification de l'adresse
	if(empty($_POST['address']))
	{
		$results["error"] = true;
		$results["message"]["address"] = "Veuillez renseigner votre adresse";
	}
	//Vérification de l'adresse mail
	if(empty($_POST['mail']))
	{
		$results["error"] = true;
		$results["message"]["mail"] = "Veuillez renseigner votre adresse mail";
	}
	else if(!filter_var($_POST['mail'],FILTER_VALIDATE_EMAIL))
	{
		$results["error"] = true;
		$results["message"]["mail"] = "Adresse mail invalide";
	}
	//Vérification des mots de passe
	if(empty($_POST['password']))
	{
		$results["error"] = true;
		$results["message"]["password"] = "Veuillez renseigner votre mot de passe";
	}
	if(empty($_POST['passwordConfirm']))
	{
		$results["error"] = true;
		$results["message"]["passwordConfirm"] = "Veuillez renseigner à nouveau votre mot de passe";
	}
	if($_POST['password'] != $_POST['passwordConfirm'])
	{
		$results["error"] = true;
		$results["message"]["passwordConfirm"] = "Les deux mots de passe sont différents";
	}
	
	
	//Si pas d'erreur de vérification des champs, on peut continuer
	if($results['error'] == false)
	{
		$prenom = $_POST['prenom'];
		$nom = $_POST['nom'];
		$birthdate = $_POST['birthdate'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$mail = $_POST['mail'];
		$password = $_POST['password'];
		
		//Il faut vérifier que l'utilisateur n'existe pas déjà dans la base de données grâce à son adresse mail
		//On ne veut pas avoir 2 fois le même utilisateur dans la base de données.
		$requete = $db->prepare("SELECT user_id FROM user WHERE email = :mail");
		$requete->execute([':mail' => $mail]);
		$row = $requete->fetch();
		if($row)
		{
			$results["error"] = true;
			$results["message"]["mail"] = "Cette adresse mail est déjà utilisée";
		}
		/*else
		{
			$results['id'] = $row->user_id;
		}*/
		
		//Sinon on peut l'insérer dans la BDD
		if($results["error"] == false)
		{
			$password = password_hash($password, PASSWORD_BCRYPT);

			$sql = $db->prepare("INSERT INTO user(first_name,last_name,birth_date,phone_nb,address,email,password) VALUES(:prenom,:nom,:birthdate,:phone,:address,:mail,:password)");
		
			$sql->execute([":prenom" => $prenom ,":nom" => $nom ,":birthdate" => $birthdate,":phone" => $phone,":address" => $address,":mail" => $mail,":password" => $password]);
			
			$sql = $db->prepare("SELECT user_id FROM user WHERE email = :mail");
			$sql->execute([":mail" => $mail]);
			$rows = $sql->fetch(PDO::FETCH_ASSOC);
			$results['id'] = $rows['user_id']; 
			
			if(!$sql)
			{
				$results["error"] = true;
				$results["message"] = "Erreur lors de linscription";
			}
		}
	}
	
	echo json_encode($results);
}

?>