<?php

	//Connexion à la base de données
	$db = new PDO("mysql:host=localhost;dbname=projet_archi","root","");
	$results["error"] = false;
	$results["message"] = [];
	
	
	/*$_POST['user_id']= 9;
	$_POST['article_id']= 4;
	$_POST['amount'] = 2;
	$_POST['shop_id']= 3;
	$_POST['delivry_address']= "ezqdsdfgdrghdfg";
	$_POST['delivry_date']= "efdsfgdfhgfnhf";*/
	
	if(isset($_POST))
	{
		if(!empty($_POST['user_id']) && !empty($_POST['article_id']) && !empty($_POST['amount']) && !empty($_POST['shop_id']) && !empty($_POST['delivry_address']) && !empty($_POST['delivry_date']))
		{
			$user_id = intval($_POST['user_id']);
			$article_id = intval($_POST['article_id']);
			$amount = intval($_POST['amount']);
			$shop_id = intval($_POST['shop_id']);
			$delivry_address = $_POST['delivry_address'];
			$delivry_date = $_POST['delivry_date'];
			
			$sql = $db->prepare("INSERT INTO orders(user_id,article_id,amount,shop_id,delivry_address,delivry_date) VALUES(:user_id,:article_id,:amount,:shop_id,:delivry_address,:delivry_date)");
			$sql->execute([":user_id" => $user_id ,":article_id" => $article_id ,"amount" => $amount, ":shop_id" => $shop_id,":delivry_address" => $delivry_address,":delivry_date" => $delivry_date]);
			$results["message"] = "réussi";
			if(!$sql)
			{
				$results["error"] = true;
				$results["message"] = "Erreur lors de l ajout";
			}
			
		}
		else
		{
			$results["error"] = true;
			$results["message"] = "Il manque des informations";
		}
	
		echo json_encode($results);
	}
	

?>