-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 07 déc. 2021 à 21:23
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projet_archi`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`article_id`),
  KEY `idShopArticle` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`article_id`, `nom`, `shop_id`, `price`, `description`) VALUES
(1, 'Biere', 1, 30, 'Fut de 6L'),
(2, 'Pina Colada', 1, 15, 'Cocktail maison dans un contenant de 1 litre'),
(3, 'Cocktail C3PO', 3, 20, 'Jus de mangue, jus d\'ananas, jus de pomme, extrait de vanille en contenant de 1.5L'),
(4, 'Jack Cherriper', 3, 25, 'Jack Daniel\'s, liqueur Cherryn, jus exotique, ananas caramelise en contenant de 1.5L');

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `delivry_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivry_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `idUtilisateur_idx` (`user_id`),
  KEY `idArticle_idx` (`article_id`),
  KEY `idShop_idx` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `shop`
--

DROP TABLE IF EXISTS `shop`;
CREATE TABLE IF NOT EXISTS `shop` (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `shop`
--

INSERT INTO `shop` (`shop_id`, `name`, `address`, `description`, `lat`, `lon`) VALUES
(1, 'Shamrock', '20B Av. Robert Schuman, 57000 Metz', 'Un bar cool', 49.11354471255548, 6.172187372900664),
(3, 'Vivian\'s Pub', '15 Place St Louis, 57000 Metz', 'Ce pub propose bieres, cidre, snacking et concerts dans un decor medieval pittoresque entre pierre et bois', 49.11709231223628, 6.178795811733135);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_nb` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birth_date`, `phone_nb`, `address`, `email`, `password`) VALUES
(3, 'Coline', 'Raggini', '29/01/1999', '0626849865', '7 rue Victor Hugo, 57290 Seremange-Erzange', 'coline57240@live.com', '$2y$10$A81PhLQuiC5CeEzF48/0necaz8Uv1cL2pfx99FZ5TuhyIXWyxgJdK'),
(9, 'Sherlock', 'Holmes', '29/01/1999', '0606060606', '221B Baker Street', 'sherlock.holmes@gmail.com', '$2y$10$KTZ9tOPDjzPerujfaGKzUOUE2nU/X8g9a3GZSUcMpI4/CvO2UPn6y'),
(28, 'Sherlock', 'Holmes', '29/01/1999', '0606060606', '221B Baker Street', 'she.ho@gmail.com', '$2y$10$mFBwXj3Il9aHPWQF3/DcW.4Tuo4sSHRYneXpef8UF1OZ.TbGaZT3S'),
(29, 'ee', 'ee', '09/09/1999', '0000000000', 'all', 'em.is@gmail.com', '$2y$10$jRcnPzAHR./SNpZyzjimNOiuks13KR3G1FDr3cmMm8agych1s3e9S'),
(30, 'ee', 'ee', '08/08/1999', '0000000000', 'so', 'si.da@gmail.com', '$2y$10$/IFfDPGYsaYyFUi0jCHEReS/9ECcXgEhnhxu6aQZG3j1QYI1U0NtC');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `idShopArticle` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `idArticle` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idShop` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idUtilisateur` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
